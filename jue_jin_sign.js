
const $ = new Env('掘金签到')

// Nodemailer是一个简单易用的Node.js邮件发送组件
const nodeMailer = require('nodemailer');
// 易用、简洁且高效的http库
const axios = require('axios');
// 请求签到、抽奖的接口
const checkInApi = "https://api.juejin.cn/growth_api/v1/check_in"
const drawApi = "https://api.juejin.cn/growth_api/v1/lottery/draw"
// 请求接口的cookie配置 cookie的获取见下面的图片说明
const cookieInfo = `_ga=GA1.2.598232678.1645581768; MONITOR_WEB_ID=dbfbb5a3-0d17-4e08-a898-3e982b68669b; __tea_cookie_tokens_2608=%257B%2522web_id%2522%253A%25227067719849715582501%2522%252C%2522user_unique_id%2522%253A%25227067719849715582501%2522%252C%2522timestamp%2522%253A1645581768087%257D; _tea_utm_cache_2608={%22utm_medium%22:%2210%22%2C%22utm_campaign%22:%22kp%22}; _gid=GA1.2.473343960.1656898930; passport_csrf_token=43c5098f627994a6e35a65585614a65c; passport_csrf_token_default=43c5098f627994a6e35a65585614a65c; _tea_utm_cache_2018=undefined; n_mh=myENDrMHWJu_RbB8ofgQDedV9VJUdBdwhyh0iSQEXLU; passport_auth_status=a4ffc6a1773ff76ccc76581adc6cb280%2C; passport_auth_status_ss=a4ffc6a1773ff76ccc76581adc6cb280%2C; sid_guard=f4671cc50dae60cdad92b9fd2fbf15b0%7C1657078804%7C31536000%7CThu%2C+06-Jul-2023+03%3A40%3A04+GMT; uid_tt=30625ce0026b7b34dc6e3ff029cf4519; uid_tt_ss=30625ce0026b7b34dc6e3ff029cf4519; sid_tt=f4671cc50dae60cdad92b9fd2fbf15b0; sessionid=f4671cc50dae60cdad92b9fd2fbf15b0; sessionid_ss=f4671cc50dae60cdad92b9fd2fbf15b0; sid_ucp_v1=1.0.0-KDdkMTgyMjc4NGFhY2U5N2E5M2YzNGYzNzhhZWRlYjZkMjE5YWU1ZDMKFwiXw-Dp1Iy4AxCUiJSWBhiwFDgCQPEHGgJsZiIgZjQ2NzFjYzUwZGFlNjBjZGFkOTJiOWZkMmZiZjE1YjA; ssid_ucp_v1=1.0.0-KDdkMTgyMjc4NGFhY2U5N2E5M2YzNGYzNzhhZWRlYjZkMjE5YWU1ZDMKFwiXw-Dp1Iy4AxCUiJSWBhiwFDgCQPEHGgJsZiIgZjQ2NzFjYzUwZGFlNjBjZGFkOTJiOWZkMmZiZjE1YjA`
// 发送邮件的配置
// user、from、to都填写自己的qq邮箱, pass的获取见下面的图片说明
const emailInfo =  {
    "user": "xxx@qq.com",
    "from": "xxx@qq.com",
    "to": "xxx@qq.com",
    "pass": "xxx"
}
// 请求签到接口
const checkIn = async () => {
    let {data} = await axios({url: checkInApi, method: 'post', headers: {Cookie: cookieInfo}});
    return data
}
// 请求抽奖接口
const draw = async () => {
    let {data} = await axios({ url: drawApi, method: 'post', headers: { Cookie: cookieInfo } });
    return data
}
// 签到完成 发送邮件
const sendQQEmail = async (subject, html) => {
    let {user, from, to, pass } = emailInfo;
    const transporter = nodeMailer.createTransport({ service: 'qq', auth: { user, pass } });
    transporter.sendMail({ from, to, subject, html },  (err) => {
        if (err) return console.log(`发送邮件失败：${err}`);
        console.log('发送邮件成功')
    })
}
// 触发签到和抽奖的方法
exports.signIn = async () => {
    const checkInData = await checkIn();
    const drawData = await draw();
    console.log('🔥', checkInData, drawData)
    if(checkInData.data && drawData.data) {
        sendQQEmail('掘金签到和抽奖成功', `掘金签到成功！今日获得${checkInData.data.incr_point}积分，当前总积分：${checkInData.data.sum_point}。 掘金免费抽奖成功, 获得：${drawData.data.lottery_name}`);
    } else if(checkInData.data && !drawData.data) {
        sendQQEmail('掘金签到成功, 抽奖失败', `掘金签到成功！今日获得${checkInData.data.incr_point}积分，当前总积分：${checkInData.data.sum_point}。 掘金免费抽奖失败, ${JSON.stringify(drawData)}`);
    }  else if(!checkInData.data && drawData.data) {
        sendQQEmail('掘金签到失败, 抽奖成功', `掘金签到失败！${JSON.stringify(checkInData)}。 掘金免费抽奖成功, 获得：${drawData.data.lottery_name}`);
    } else if(!checkInData.data && !drawData.data) {
        sendQQEmail('掘金签到和抽奖失败', `掘金签到失败！${JSON.stringify(checkInData)}。 掘金免费抽奖失败, ${JSON.stringify(drawData)}`);
    }
};
