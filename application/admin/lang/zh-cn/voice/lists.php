<?php

return [
    'Id'            => 'ID',
    'Name'          => '主播名称',
    'Param'         => '参数名',
    'Label'         => '使用场景',
    'Status'        => '状态',
    'Status 0'      => '不启用',
    'Status 1'      => '启用',
    'Text'          => '试听内容',
    'Category_id'   => '分类',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间',
    'Deletetime'    => '删除时间',
    'Category.name' => '分类名称'
];
