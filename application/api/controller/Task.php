<?php

namespace app\api\controller;

use app\common\controller\Api;
use fast\Random;

/**
 * Token接口
 */
class Task extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = '*';

    public function list(){
        $model = new \app\common\model\exec\Lists();
        $page = $this->request->get("page");
        $data = $model->where('user_id',$this->auth->id)->order('id','DESC')->page($page,10)->select();
        $total = $model->where('user_id',$this->auth->id)->count();
        return $this->success("成功",[
            'data'=>$data,
            "page"=> [
              "totalRows"=>$total
            ]
        ]);
    }
}